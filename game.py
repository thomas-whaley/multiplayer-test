from typing import List

from updateable import Updatable
from player import Player

class Game(Updatable):
    players: List[Player]

    def update(self) -> None:
        for player in self.players:
            player.update()
    