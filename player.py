from enum import Enum

from updateable import Updatable

class PlayerStatus(str, Enum):
    ACTIVE = 'ACTIVE'
    LEFT = 'LEFT'

class Player(Updatable):
    x: float
    y: float
    player_id: int
    status: PlayerStatus

    def __init__(self, x: float, y: float, player_id: int = 0, status: PlayerStatus = PlayerStatus.ACTIVE) -> None:
        self.x = x
        self.y = y
        self.player_id = player_id
        self.status = status

    def update(self) -> None:
        pass

    def move(self, x: float, y: float) -> None:
        self.move_to(self.x + x, self.y + y)
    
    def move_to(self, x: float, y: float) -> None:
        self.x = x
        self.y = y
    
    def deinit(self) -> None:
        pass