from abc import abstractmethod
from dataclasses import dataclass
from typing import List

from player import Player

import pygame

class Renderable:
    @abstractmethod
    def render(self, window: pygame.surface.Surface) -> None:
        pass

class Window:
    width: int
    height: int

    name: str

    background: pygame.color.Color

    running: bool = True

    window: pygame.surface.Surface

    children: List[Renderable]

    def __init__(self, width: int, height: int, name: str, background=pygame.color.Color(0, 0, 0)) -> None:
        self.width = width
        self.height = height
        self.name = name
        self.background = background
        self.window = pygame.display.set_mode(size=(self.width, self.height))
        pygame.display.set_caption(self.name)
        self.children = []

    def add_child(self, child: Renderable) -> None:
        self.children.append(child)
    
    def remove_child(self, child: Renderable) -> None:
        if child in self.children:
            self.children.remove(child)

    def render(self) -> None:
        self.window.fill(self.background)

        for child in self.children:
            child.render(self.window)

        pygame.display.flip()
    
    def events(self) -> None:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False

@dataclass
class PlayerRenderer(Renderable):
    player: Player

    def render(self, window: pygame.surface.Surface) -> None:
        pygame.draw.rect(window, (255, 255, 255), (int(self.player.x), int(self.player.y), 50, 50))
