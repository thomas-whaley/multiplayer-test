from typing import List, Dict, Optional

from drawing import Window, PlayerRenderer
from player import Player, PlayerStatus
from controller import ClientControllablePlayer

window = Window(500, 400, "Test")

class Game:
    window: Window
    controllable: Optional[ClientControllablePlayer]
    players: Dict[int, Player] = {}

    __player_renderer: Dict[int, PlayerRenderer] = {}

    def __init__(self, window: Window) -> None:
        self.window = window

    def add_player(self, player: Player) -> None:
        self.players[player.player_id] = player
    
    def remove_player(self, player: Player) -> None:
        if player.player_id not in self.players:
            return
        del self.players[player.player_id]
    
    def add_player_to_window(self, player: Player) -> None:
        self.__player_renderer[player.player_id] = PlayerRenderer(player)
        self.window.add_child(self.__player_renderer[player.player_id])
    
    def remove_player_from_window(self, player: Player) -> None:
        if player.player_id not in self.__player_renderer:
            return
        self.window.remove_child(self.__player_renderer[player.player_id])
        del self.__player_renderer[player.player_id]
    
    def set_controllable_player(self, player: Player, speed: float) -> None:
        self.controllable = ClientControllablePlayer(player, speed)
    
    def update_client(self) -> None:
        if not self.controllable:
            return
        for msg in self.controllable.client.recieve_message():
            player = self.controllable.parse_msg(msg)
            if not player:
                continue
            if player.player_id == self.controllable.player.player_id:
                continue
            
            if player.status == PlayerStatus.LEFT:
                self.remove_player(player)
                self.remove_player_from_window(player)
                continue

            if player.status != PlayerStatus.ACTIVE:
                continue

            if player.player_id not in self.players:
                self.controllable.update_server()
                self.add_player(player)
                self.add_player_to_window(player)
            self.players[player.player_id].move_to(player.x, player.y)
    
    def deinit(self) -> None:
        for player in self.players.values():
            player.deinit()
        if self.controllable:
            self.controllable.deinit()

    def main(self) -> None:
        while self.window.running:
            self.window.render()
            self.window.events()

            for player in self.players.values():
                player.update()
            
            self.update_client()
            
            if self.controllable:
                self.controllable.update()
        self.deinit()


main = Game(Window(500, 400, "Test"))

player: Player = Player(50, 50)

main.add_player(player)
main.add_player_to_window(player)
main.set_controllable_player(player, 0.1)

main.main()

    