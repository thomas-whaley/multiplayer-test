from abc import abstractmethod

class Updatable:
    @abstractmethod
    def update(self) -> None:
        pass

    @abstractmethod
    def deinit(self) -> None:
        pass