import json
import math
from typing import Optional

from pygame.key import get_pressed
from pygame import K_a, K_d, K_w, K_s

from updateable import Updatable
from player import Player, PlayerStatus
from server.client import Client


class Controllable(Updatable):
    pass

class ControllablePlayer(Controllable):
    player: Player
    player_speed: float

    def __init__(self, player: Player, player_speed: float) -> None:
        self.player = player
        self.player_speed = player_speed

    def update(self) -> None:
        keys = get_pressed()
        if keys[K_a]:
            self.player.move(-self.player_speed, 0)
        if keys[K_d]:
            self.player.move(self.player_speed, 0)
        if keys[K_w]:
            self.player.move(0, -self.player_speed)
        if keys[K_s]:
            self.player.move(0, self.player_speed)

class ClientControllablePlayer(ControllablePlayer):
    client: Client

    __prev_pos_x: float = 0
    __prev_pos_y: float = 0
    __send_threshold: float = 10

    def __init__(self, player: Player, player_speed: float) -> None:
        super().__init__(player, player_speed)
        self.client = Client()
        self.player.player_id = self.client.unique_id
        self.update_server()

    def update_server(self) -> None:
        self.client.send_message(self.to_msg())

    def to_msg(self) -> str:
        return json.dumps(vars(self.player))

    def leave_message(self) -> str:
        msg = json.loads(self.to_msg())
        msg['status'] = PlayerStatus.LEFT
        return json.dumps(msg)
    
    def parse_msg(self, msg: str) -> Optional[Player]:
        loaded = json.loads(msg)
        if 'x' not in loaded or 'y' not in loaded or 'player_id' not in loaded or 'status' not in loaded:
            return None
        player = Player(0, 0)
        player.__dict__.update(loaded)
        return player

    def update(self) -> None:
        super().update()
        if math.hypot(self.player.x - self.__prev_pos_x, self.player.y - self.__prev_pos_y) > self.__send_threshold:
            self.update_server()
            self.__prev_pos_x = self.player.x
            self.__prev_pos_y = self.player.y
    
    def deinit(self) -> None:
        self.client.send_message(self.leave_message())

