from typing import List
import socket

from server.handy import HOST, PORT, END_DELIMITER

class Client:
    def __init__(self) -> None:
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((HOST, PORT))
        self.unique_id = int(self.socket.recv(16))
        self.socket.settimeout(1e-7)
    
    def send_message(self, data: str) -> None:
        output = (data + END_DELIMITER).encode()
        self.socket.send(output)

    def recieve_message(self) -> List[str]:
        try:
            data = self.socket.recv(1024).decode()
        except socket.timeout as e:
            return []
        except socket.error as e:
            raise Exception(e)
        if len(data) == 0:
            raise Exception("Orderly shutdown on server end")
        return [x for x in data.split(END_DELIMITER) if x]

    def close(self) -> None:
        self.socket.close()