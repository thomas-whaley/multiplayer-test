import socket               # Import socket module
import _thread

from handy import HOST, PORT

clients = []
number_of_clients = 0

def on_new_client(clientsocket,addr):
    global number_of_clients
    print(f'New connection: {addr}')
    clients.append(clientsocket)
    number_of_clients += 1
    print(f'Giving new connection unique id of: {number_of_clients}')
    clientsocket.send(str(number_of_clients).encode())
    while True:
        try:
            msg = clientsocket.recv(1024)
        except ConnectionResetError:
            break
        if not msg:
            break
        # print(f"\t{addr} > {msg!r}")
        for client in clients:
            try:
                sender = client.getpeername()
                # print(f"\t\tRelay > {sender}")
            except OSError:
                pass
            client.send(msg)
    print(f"Closed connection: {addr}")
    clients.remove(clientsocket)
    clientsocket.close()

s = socket.socket()         # Create a socket object

print('Server started!')
print('Waiting for clients...')

s.bind((HOST, PORT))        # Bind to the port
s.listen(5)                 # Now wait for client connection.

while True:
    c, addr = s.accept()     # Establish connection with client.
    _thread.start_new_thread(on_new_client,(c,addr))
s.close()